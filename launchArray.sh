#!/bin/sh
#SBATCH -J wallis
#SBATCH -p mmmi -q mmmi
#SBATCH --mem-per-cpu=60000
#SBATCH --cpus-per-task=1
#SBATCH --array=1-276
#SBATCH -e ./log/out_%A_%a.err
#SBATCH -o ./log/out_%A_%a.log





module load R/4.1.0

cd /pasteur/zeus/projets/p01/MMMICovid/abrault/codes/wallisFutuna/

srun Rscript runScenarios.R ${SLURM_ARRAY_TASK_ID}

exit 0
