# Impact of test and treat with nirmatrelvir/ritonavir to mitigate the epidemic rebound when Zero-COVID ends in Wallis and Futuna

Data and source code used in the preprint by Brault et al. [Impact of test and treat with nirmatrelvir/ritonavir to mitigate the epidemic rebound when Zero-COVID ends in Wallis and Futuna](https://hal.archives-ouvertes.fr/hal-03770270).

## Requirements

You need to install `R` and `RStudio` with the following packages:

- `odin`
- `tidyverse`
- `lubridate`
- `ggpubr`
- `readxl`
- `furrr`
- `ggrepel`
- `RColorBrewer`
- `egg`

## Run the baseline scenario

You can run the baseline scenario of the article with `main.R`.

## Run all scenarios to reproduce the results of the article

The simulation of all the scenarios is demanding in terms of computational and storage resources.
The use of a cluster is recommended. We describe here how to run all scenarios with Slurm.

1. Modify the line `cd ...` in the file `launchArray.sh` to indicate your working directory.

2. Create folders to store results:

```
$  mkdir -p output/results/cossScenariosCluster output/results/cossScenariosStats
```

3. Then you can run all the scenarios of the article in a Slurm cluster with the command:

```
$ sbatch launchArray.sh
```

4. Merge the statistics of all scenarios with `reconstructCrossResults.R`.

5. Plot the results with `plotStatsRes.R`. The plots are saved in subfolders of `output/figures/` that must be created before running the script.

## Data

All data needed to reproduce the results of the article can be found in the folder [data/](data/):

- [comorbidities/](data/comorbidities/) contains:

  - [comorbFranceEnqueteEsteban.xlsx](data/comorbidities/comorbFranceEnqueteEsteban.xlsx) prevalence of comorbidities in France (source: private communication with Santé Publique France)
  - [riskGroup5years.csv](data/comorbidities/riskGroup5years.csv) prevalence of comorbidities in Wallis and Futuna (source: private communication with Agence Départementale de Santé de Wallis et Futuna, coming from [STEPS 2019](https://cdn.who.int/media/docs/default-source/ncds/ncd-surveillance/data-reporting/wallis-and-futuna/wallis_futuna_steps_2019_report_print.pdf?sfvrsn=f0e5d71b_1&download=true))

- [contactMatrix/](data/contactMatrix/) contains:

  - [contactMatrixFiji.rds](data/contactMatrix/) contact matrix in Fiji (source: [link github](https://github.com/kieshaprem/synthetic-contact-matrices))
  - [contactMatrixWFDemoCorr.rds](data/contactMatrix/) contact matrix in Wallis and Futuna computed from Fiji contact matrix and corrected with the demography in Wallis and Futuna (see [Supplementary Material](https://hal.archives-ouvertes.fr/hal-03770270)). The files [contactMatrixWFSym.rds](data/contactMatrix/) and [contactMatrixWFNorm.rds](data/contactMatrix/) are contact matrices of Wallis and Futuna calculated with other formulas, but finally not used.

- [epidemic/](data/epidemic/) contains:

  - [casesByAgeWF.csv](data/epidemic/casesByAgeWF.csv): cases by age in Wallis and Futuna during the first wave of Covid-19 in 2021
  - [epidemicWallisFutuna.csv](data/epidemic/epidemicWallisFutuna.csv): cases (`cases`), hospital admissions (`hospi`), ICU admissions (`icu`), and deaths (`deaths`) each day of the first wave of Covid-19 in 2021
  - [casesHospiWave2WF.csv](data/epidemic/casesHospiWave2WF.csv): daily cases by date of symptoms (`symp_date`), daily cases by date of test (`test_date`), daily hospital admissions (`hospi`), daily hospital discharges (`h_discharges`), daily number of patients treated (`n_treated`) during the second wave (after the reopening) in 2022.

- [populationData/](data/populationData/) contains demographic data by age for Fiji [populationByAgeFiji2019.csv](data/populationData/populationByAgeFiji2019.csv) and Wallis and Futuna [populationByAgeWallisFutuna2018.csv](data/populationData/populationByAgeWallisFutuna2018.csv) ([census INSEE 2018](https://www.insee.fr/fr/statistiques/4219031)).

- [severityEstimates/](data/severityEstimates/) contains:
  - [chrObsWF.csv](data/severityEstimates/chrObsWF.csv): case hospitalisation rate (CHR) (hospital admissions/ cases) observed during the first wave of Covid-19 in 2021 in Wallis and Futuna.
  - [ihrFranceAncestralStain.xlsx](data/severityEstimates/): infection hospitalisation rate (IHR) by age in metropolitan France estimated in [Lapidus et al.](https://pubmed.ncbi.nlm.nih.gov/33521775/) for the ancestral strain of SARS-CoV-2.
  - [ihr0WF.csv](data/severityEstimates/ihr0WF.csv): IHR by age estimated in Wallis and Futuna for people without comorbidities.
  - [ihrWF.csv](data/severityEstimates/ihrWF.csv): IHR by age estimated in Wallis and Futuna taking into account of prevalence of comorbidities.
- [vaccination/](data/vaccination/) contains
  - [vaccinationByAgeGroupWallisFutunaRealDoses.csv](data/vaccination/vaccinationByAgeGroupWallisFutunaRealDoses.csv): vaccination coverage by age group mid-May 2022 in Wallis and Futuna.
  - [vaccinationByAgeGroupWallisFutuna.csv](data/vaccination/vaccinationByAgeGroupWallisFutuna.csv): vaccination coverage by age group mid-May 2022 in Wallis and Futuna taken into account previous infections.

## Scripts

- [main.R](main.R): run the baseline scenario.
- [launchArray.sh](launchArray.sh): run all scenarios in a Slurm cluster.
- [reconstructCrossResults.R](reconstructCrossResults.R): merge all statistics of scenarios stored in `output/results/cossScenariosStats/` in one file `output/results/crossScenariosStatAll.rds`.
- [plotStatsRes.R](plotStatsRes.R): plot figures of the article, they are saved in `output/figures/`.
- [runScenario.R](runScenarios.R): run the scenario of index `i` in [scenarioParams/crossScenariosParams.csv](scenarioParams/crossScenariosParams.csv) with the command
  `Rscript runScenarios.R i`.
- [vaccinationScenario.R](vaccinationScenario.R): create different vaccination scenarios and save them in [scenarioParams/](scenarioParams/).
- [defineScenarios.R](defineScenarios.R): create [scenarioParams/crossScenariosParams.csv](scenarioParams/crossScenariosParams.csv) with all scenarios of the article.
- [exploreData.R](exploreData.R): explore the data about the first wave of Covid-19 in Wallis and Futuna, prevalence of comorbidities, contact matrices.
- [epidemicFollow.R](epidemicFollow.R): compare predictions of the model with observed cases, hospital admissions, patients treated in Wallis and Futuna after reopening in June 2022.
- [utils/](utils/) contains:
  - [odinFunctions.R](utils/odinFunctions.R): functions to run the compartmental model stored in [odinModels/modelBoostComorb.R](odinModels/modelBoostComorb.R).
  - [scriptsForMatrixSymmetrization.R](utils/scriptsForMatrixSymmetrization.R): functions to manipulate contact matrices.
  - [sensitivityAnalysis.R](utils/sensitivityAnalysis.R): run a sensitivity analysis according to one parameter.
  - [utilsBeds.R](utils/utilsBeds.R): functions to compute beds from admissions.
  - [utilsIHR.R](utils/utilsIHR.R): function to compute IHR in Wallis and Futuna with the relative risk of hospitalisation
    in comorbidities groups.
